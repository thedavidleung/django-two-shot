from django.shortcuts import render
from django.views.generic import ListView
from receipts.models import Receipt
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic import CreateView
from django.shortcuts import redirect
from receipts.models import ExpenseCategory
from receipts.models import Account


# Create your views here.
class ReceiptListView(LoginRequiredMixin, ListView):
    model = Receipt
    template_name = "receipts/list.html"
    context_object_name = "receiptlist"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user)


class ReceiptCreateView(LoginRequiredMixin, CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    context_object_name = "receiptcreate"
    fields = ["vendor", "total", "tax", "date", "category", "account"]

    def form_valid(self, form):
        receipt = form.save(commit=False)
        receipt.purchaser = self.request.user
        receipt.save()
        return redirect("home")


class ExpenseCategoryListView(LoginRequiredMixin, ListView):
    model = ExpenseCategory
    template_name = "expensecategories/list.html"
    context_object_name = "expensecategorieslist"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class AccountListView(LoginRequiredMixin, ListView):
    model = Account
    template_name = "accounts/list.html"
    context_object_name = "accountlist"

    def get_queryset(self):
        return Account.objects.filter(owner=self.request.user)
